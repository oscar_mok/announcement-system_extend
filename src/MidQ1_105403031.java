import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.List;

/*
 * 資管4A
 * 105403031  莫智堯
 *
 * Login password: 1234
 */

public class MidQ1_105403031 extends JFrame {

    private JButton new_post_button;
    private JPanel button_holder_main_screen;
    private JPanel read_area_JPanel;
    private JPanel buttons_right_hand_side_holder;
    private boolean isLike = false;
    private String content;
    private JLabel content_JLabel;
    private JPanel header_JPanel;
    private JLabel header_JLabel1, header_JLabel2;
    private JButton saveAs_button, save_button, import_button, cancel_button;
    private JPanel edit_mode_Btn_holder;
    private JTextArea content_JTextArea;
    private String edit_time;
    private java.util.Date date;
    private Date edit_time_previous_edit_date;
    private boolean new_post_Btn_clicked;
    private JPanel head_JComboBox_n_JLabel_holder;
    private JComboBox<String> posts_comboBox;
    private String combo_box_file_array[] = {"post"};
    private String selected_fileName_temp;
    private JComboBox<String> posts_comboBox_temp;

    public MidQ1_105403031(){

        String[] options = {"是(Y)", "否(N)", "取消"};

        int result = JOptionPane.showOptionDialog(this, "Are you pharmacist?", "Login", JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE, null, options, "是(Y)");


        if (result == 0){ //選"是(Y)"

            publisher_mode(); // function內 實作發佈者模式

        }else if (result == 1){ //選"否(N)"

            non_publisher_mode(); // 非發佈者模式

        }else { //按 "取消" 或 按"x" 關閉視窗

            Runtime.getRuntime().exit(0); //結束程式
        }
    }

    private void non_publisher_mode() {
        read_area_functions(); //Function內實作 發佈者模式 / 非發佈者模式 會共同出現的功能
    }

    private void publisher_mode() {
        read_area_functions(); //Function內實作 發佈者模式 / 非發佈者模式 會共同出現的功能

        buttons_right_hand_side_holder = new JPanel();

        //指定背景顏色
        buttons_right_hand_side_holder.setBackground(Color.pink);
        //

        new_post_button = new JButton("new");
        new_post_button.setPreferredSize(new Dimension(85,40));
        new_post_Btn_onClick();

        //把button 加到buttons_right_hand_side_holder 裡面
        buttons_right_hand_side_holder.add(new_post_button);
        //


        button_holder_main_screen.add(buttons_right_hand_side_holder, BorderLayout.EAST); //用borderLayout 設定align 在右邊
    }

    private void edit_mode_Btn_initialize() {
        edit_mode_Btn_holder = new JPanel();

        edit_mode_Btn_holder.setBackground(Color.pink);

        date = new java.util.Date();
        header_JLabel2.setText(date.toString()); //進入編輯模式時 更改header 的時間
        posts_comboBox.setEnabled(false);

        save_button = new JButton("儲存"); //建立button
        saveAs_button = new JButton("另存內容");
        import_button = new JButton("匯入內容");
        cancel_button = new JButton("取消");

        save_button.setPreferredSize(new Dimension(85, 40)); //設定button 大小
        saveAs_button.setPreferredSize(new Dimension(85, 40));
        import_button.setPreferredSize(new Dimension(85, 40));
        cancel_button.setPreferredSize(new Dimension(85, 40));

        edit_mode_Btn_holder.add(save_button);
        edit_mode_Btn_holder.add(saveAs_button);
        edit_mode_Btn_holder.add(import_button);
        edit_mode_Btn_holder.add(cancel_button);

        button_holder_main_screen.add(edit_mode_Btn_holder, BorderLayout.WEST);
    }

    private void edit_mode_Btn_Onclick() {
        save_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try
                {
                    File file_post = new File("src/" + selected_fileName_temp);

                    System.out.println(file_post.toPath());
                    ObjectOutputStream output = new ObjectOutputStream(
                            Files.newOutputStream(file_post.toPath())); //打開"post" 檔案

                    try
                    {
                        // 連到PostSerializable.java
                        PostSerializable record = new PostSerializable();

                        record.setContent(content_JTextArea.getText());
                        record.setEditTime(date);
                        record.setIsLike(isLike);

                        // serialize record object into file
                        output.writeObject(record);

                        output.close();

                        return_front_page(); //執行function 回到主畫面
                    }
                    catch (IOException ioException)
                    {
                        System.err.println("Error writing to file.");
                        JOptionPane.showMessageDialog(null, "Error writing to file.");
                    }
                }
                catch (IOException ioException)
                {
                    System.err.println("Error while saving file.");
                    JOptionPane.showMessageDialog(null, "Error while saving file.");
                }
            }
        });

        saveAs_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String filename = JOptionPane.showInputDialog("另存新檔名稱：");
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setSelectedFile(new File(filename)); //把用戶輸入的檔案名稱設定好
                int result = fileChooser.showSaveDialog(null);  //顯示儲存panel

                if (result == JFileChooser.APPROVE_OPTION){ //如果User點擊"儲存"button
                    try{
                        //fileChooser.getSelectedFile() <- 讀取要寫入的檔案位置＋檔案名稱
                        BufferedWriter writer = new BufferedWriter(new FileWriter(fileChooser.getSelectedFile()));

                        writer.write(content_JTextArea.getText()); //把輸入的文字寫到檔案裡
                        writer.close();
                        JOptionPane.showMessageDialog(null, "檔案已成功儲存!","訊息",JOptionPane.INFORMATION_MESSAGE);

                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }else {
                    System.out.println("Cancel button pressed.");
                }
            }
        });

        import_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
/*
//              !!!!!! Using ObjectInputStream !!!!!!!!


                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(
                        JFileChooser.FILES_AND_DIRECTORIES); //設定 可以讀取檔案&資料夾
                int result = fileChooser.showOpenDialog(null); //彈出開檔案介面

                // if user clicked Cancel button on dialog
                if (result == JFileChooser.CANCEL_OPTION) { //如果按 取消

                    System.out.println("Cancel button pressed.");

                }else { //按了 匯入

                    // Path representing the selected file
                    Path file_path = fileChooser.getSelectedFile().toPath();

                    try {
                        ObjectInputStream input = new ObjectInputStream(Files.newInputStream(file_path));  //ObjectInputStream 可以把Byte code 轉回文字

                        //ObjectInputStream沒有error 就跑以下這段
                        try {
                            while (true) // loop until there is an EOFException
                            {
                                PostSerializable file_record = (PostSerializable) input.readObject(); //利用PostSerializable.java 讀取檔案內的內容

                                // 把內容抓到local variable "content" 裡
                                content = file_record.getContent();
                                edit_time = file_record.getEditTime().toString();
                                isLike = file_record.getIsLike(); //更改愛心button 的狀態

                                header_JLabel2.setText(edit_time); //更新時間，改為上一次編輯的時間
                                content_JTextArea.setText(content); //把內容寫到編輯面板(JTextArea)上
                            }
                        } catch (EOFException endOfFileException) {
                            System.out.printf("%nNo more records%n");
                        } catch (ClassNotFoundException classNotFoundException) {
                            System.err.println("Invalid object type. Terminating.");
                        } catch (IOException ioException) {
                            System.err.println("Error reading from file. Terminating.");
                        }

                    } catch (IOException ioException) {
                        System.err.println("Error opening file.");
                        JOptionPane.showMessageDialog(null, "Error opening file.");
                    }
                }

 */
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(
                        JFileChooser.FILES_AND_DIRECTORIES); //設定 可以讀取檔案&資料夾
                int result = fileChooser.showOpenDialog(null); //彈出開檔案介面

                // if user clicked Cancel button on dialog
                if (result == JFileChooser.CANCEL_OPTION) { //如果按 取消

                    System.out.println("Cancel button pressed.");

                }else { //按了 匯入

                    // 取得檔案
                    File file = new File(fileChooser.getSelectedFile().getPath());

                    StringBuilder stringBuilder = new StringBuilder(); //宣告StringBuilder
                    try {
                        Scanner scanner = new Scanner(file);

                        while (scanner.hasNextLine()){ //檢查有沒有下一行 -> 有 繼續讀取

                            stringBuilder.append(scanner.nextLine() + "\n"); //把String 串在一起

                        }
                        content_JTextArea.setText(stringBuilder.toString()); //把讀進去的內容 全部顯示在JTextArea

                    } catch (FileNotFoundException ex) {

                        System.err.println("Error opening file.");
                        JOptionPane.showMessageDialog(null, "Error opening file.");

                    }
                }
            }
        });

        cancel_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                return_front_page();

                new_post_Btn_clicked = false; //全新貼文沒有儲存，boolean 值重設為false
            }
        });
    }

    private void return_front_page() {
        edit_mode_Btn_holder.setVisible(false); //隱藏edit mode 的所有button

        //重新顯示可編輯頁面的button
        new_post_button.setVisible(true);
        //

        //換回post_comboBox，重新判斷有那些 "已建立檔案"
        posts_comboBox_temp.setVisible(false);
        posts_comboBox.setVisible(true);
        posts_comboBox.setEnabled(true);

        read_area_JPanel.setBackground(Color.gray); //底色重設為灰色

        content_JTextArea.setText(null);
        content_JTextArea.setEditable(false);
        front_page_getContent();
    }

    private void new_post_Btn_onClick() {
        new_post_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String file_name = JOptionPane.showInputDialog(null, null, "Enter filename", JOptionPane.DEFAULT_OPTION);

                //Error: NullPointerException --> 如果file_name 為Null / User 按下cancel, 315行會出現Exception in thread "AWT-EventQueue-0" java.lang.NullPointerException

                if (!file_name.matches("")) { //兩種情況會出現""  1: User沒有輸入東西  2: User 按"取消"

                    //Create temporary comboBox
                    //加入user 輸入的file_name 到comboBox
                    posts_comboBox_temp = new JComboBox<String>(combo_box_file_array);
                    posts_comboBox_temp.addItem(file_name);

                    posts_comboBox.setVisible(false);
                    head_JComboBox_n_JLabel_holder.add(posts_comboBox_temp);
                    posts_comboBox_temp.setEnabled(false);

                    //設定temporary comboBox default 的值
                    posts_comboBox_temp.setSelectedItem(file_name);

                    new_post_Btn_clicked = true; //記錄是否由全新貼文進入編輯狀態

                    content_JTextArea.setText(""); //文字格設為空白
                    content_JTextArea.setEditable(true); //設成可更改
                    content_JTextArea.setBackground(Color.white);
                    content_JTextArea.setForeground(Color.black);
                    read_area_JPanel.setBackground(Color.white);

                    content_JLabel.setVisible(false);
                    new_post_button.setVisible(false);

                    content = content_JTextArea.getText(); //Get text inputted in the text area
//                read_area_JPanel.add(content_JTextArea);

                    edit_mode_Btn_initialize();
                    edit_mode_Btn_Onclick();
                }
            }
        });
    }

    private void read_area_functions() {
        header_JPanel = new JPanel();
        read_area_JPanel = new JPanel();
        button_holder_main_screen = new JPanel();
        head_JComboBox_n_JLabel_holder = new JPanel();
        header_JLabel1 = new JLabel();
        header_JLabel2 = new JLabel();
        content_JLabel = new JLabel();
        content_JTextArea = new JTextArea();
        content_JTextArea.setEditable(false);

        read_area_JPanel.setLayout(new BorderLayout());
        header_JPanel.setLayout(new GridLayout(2,1));
        header_JPanel.setPreferredSize(new Dimension(WIDTH,120));

        head_JComboBox_n_JLabel_holder.setLayout(new FlowLayout(FlowLayout.LEFT));
        header_JLabel1.setFont(new Font("", Font.BOLD, 20)); //粗體 ＋ 字體大小 -> 20
        header_JLabel1.setText("Posts");

//        List<String> file_List = Arrays.asList(combo_box_file_array);
        //for loop to get all announcement's name
        File folder = new File("src/");
        File[] listofFiles = folder.listFiles();
        ArrayList<String> arrayList = new ArrayList<>();
        LinkedList<String> combo_box_file_array_linked_list = new LinkedList<>();

        for (File file : listofFiles){
            if (file.isFile()){
                arrayList.add(file.getName());
            }
        }

        for (int i = 0; i < arrayList.size(); i++){
            if (!(arrayList.get(i).contains(".txt") || arrayList.get(i).contains(".png") || arrayList.get(i).contains(".java") ||
                    arrayList.get(i).contains(".DS_Store"))){
                System.out.println(arrayList.get(i));
                combo_box_file_array_linked_list.add(arrayList.get(i));
            }

        }

        //!!!!!!!!!!!!!!!
        combo_box_file_array = combo_box_file_array_linked_list.toArray(new String[0]);

        posts_comboBox = new JComboBox<String>(combo_box_file_array);
        posts_comboBox.setPreferredSize(new Dimension(300, getHeight()));
        head_JComboBox_n_JLabel_holder.add(header_JLabel1);
        head_JComboBox_n_JLabel_holder.add(posts_comboBox);

        java.util.Date date = new java.util.Date();
        header_JLabel2.setFont(new Font("", Font.PLAIN, 14)); //字體大小 -> 14
        edit_time = date.toString();
        header_JLabel2.setText(edit_time);
        header_JPanel.add(head_JComboBox_n_JLabel_holder);
        header_JPanel.add(header_JLabel2);

        read_area_JPanel.setBackground(Color.gray); // 發/讀公告 範圍的顏色
        button_holder_main_screen.setBackground(Color.pink); // 指定頁面底部 背景顏色

        read_area_JPanel.add(content_JTextArea, BorderLayout.WEST); //設定BorderLayout.WEST 可以讓content_JTextArea內的文字靠左

        button_holder_main_screen.setLayout(new BorderLayout()); //設定用borderLayout 設定align 位置
        button_holder_main_screen.setPreferredSize(new Dimension(WIDTH, 50));

        posts_comboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                front_page_getContent();
            }
        });

        add(header_JPanel,BorderLayout.NORTH);
        add(read_area_JPanel, BorderLayout.CENTER); //顯示公告的JPanel 會佔據頁面中間的空間
        add(button_holder_main_screen, BorderLayout.SOUTH); //button_holder_main_screen放到頁面的底部顯示

        front_page_getContent(); // 讀入post 檔內的資料
    }

    private void front_page_getContent(){
        content_JTextArea.setBackground(Color.gray);
        content_JTextArea.setForeground(Color.yellow); //文字 -> 黃色
        content_JTextArea.setFont(new Font("", Font.PLAIN, 14));

        // 讀入post 檔內的資料
        try {
            selected_fileName_temp = posts_comboBox.getSelectedItem().toString();
            File file_Post = new File("src/" + selected_fileName_temp);

            Path file_path = file_Post.toPath();
            ObjectInputStream input = new ObjectInputStream(Files.newInputStream(file_path));  //ObjectInputStream 可以把Byte code 轉回文字

            //ObjectInputStream沒有error 就跑以下這段
            try {
                while (true) // loop until there is an EOFException
                {
                    PostSerializable file_record = (PostSerializable) input.readObject(); //利用PostSerializable.java 讀取檔案內的內容

                    // 把內容抓到local variable "content" 裡
                    content = file_record.getContent();
                    isLike = file_record.getIsLike();
                    edit_time = file_record.getEditTime().toString();
                    edit_time_previous_edit_date = file_record.getEditTime(); //以Date 型態存上次檔案的儲存時間 (設定isLike 時會用到)

                    header_JLabel2.setText(edit_time); //更新時間，改為上一次編輯的時間
                    content_JTextArea.setText(content); //把內容寫到編輯面板(JTextArea)上
                }
            } catch (EOFException endOfFileException) {
                System.out.printf("%nNo more records%n");
            } catch (ClassNotFoundException classNotFoundException) {
                System.err.println("Invalid object type. Terminating.");
            } catch (IOException ioException) {
                System.err.println("Error reading from file. Terminating.");
            }

        } catch (IOException ioException) {
            System.err.println("Error opening file.");
            JOptionPane.showMessageDialog(null, "Error opening file.");
        }
    }
}
