import javax.swing.JFrame;

/*
 * 資管4A
 * 105403031  莫智堯
 *
 * Login password: 1234
 */

public class Main extends MidQ1_105403031 {

    public static void main(String[] args){
        MidQ1_105403031 MidQ1_drawer = new MidQ1_105403031();
        MidQ1_drawer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MidQ1_drawer.setSize(700, 600);
        MidQ1_drawer.setVisible(true);
    }
}
